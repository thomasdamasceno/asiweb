# ASIWeb

## Eléments réalisés du cahier des charges.
- Création d’utilisateur et login / logout (avec leurs écrans)
- Achat/vente de cartes sur la plateforme
- Gestion de porte-monnaie
- Attribution de cartes
- Gestion de la boutique


## Pourcentage d'implication de chaque membre du groupe
- 25% - Nathan
- 25% - Corentin
- 25% - Thomas
- 25% - Adrien

## Lien Gitlab
https://gitlab.com/thomasdamasceno/asiweb/-/tree/master

## Lien vidéo Youtube
https://www.youtube.com/watch?v=cS6Z10KY6sI
