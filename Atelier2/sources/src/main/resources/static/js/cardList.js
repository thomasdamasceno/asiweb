let cardList;

$.urlParam = function(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return null;
	}
	else {
		return results[1] || 0;
	}
}

let type = $.urlParam("type");
if (type === null) type = "sell";
let path = "cards/" + type;

$.ajax({
		type: "GET",
		url: "/whoAmI",
		headers: {
			"Content-Type": "application/json"
		},
		success: function(res) {
			$('#userNameId').text(res.name);
			$('#balanceId').text(res.balance);
		}
	})


$.ajax({
	type: "GET",
	url: path,
	headers: {
		"Content-Type": "application/json"
	},
	success: function(res) {
		cardList = res;
		changeShowCardById(cardList[0].id);
		let template = document.querySelector("#row");
		for (const card of cardList) {
			let clone = document.importNode(template.content, true);

			newContent = clone.firstElementChild.innerHTML
				.replace(/{{family_name}}/g, card.family)
				.replace(/{{img_src}}/g, card.img_src)
				.replace(/{{name}}/g, card.name)
				.replace(/{{description}}/g, card.description)
				.replace(/{{hp}}/g, card.hp)
				.replace(/{{energy}}/g, card.energy)
				.replace(/{{attack}}/g, card.attack)
				.replace(/{{defense}}/g, card.defense)
				.replace(/{{price}}/g, card.price)
				.replace(/{{id}}/g, card.id)
				.replace(/{{type}}/g, type.toUpperCase())
				.replace(/{{typeTransaction}}/g, type)
			clone.firstElementChild.innerHTML = newContent;
			
			clone.querySelector("tr").setAttribute("id", "row" + card.id);
			
			let cardContainer = document.querySelector("#tableContent");
			cardContainer.appendChild(clone);

			$('#row' + card.id).on('click', function() {
				changeShowCardById(card.id);
			});
		}
	}
})

function changeShowCardById(id) {
	console.log(id);
	let card = cardList.find(x => x.id === id);
	$("#card").load("./part/card-full.html", function() {
		let template = document.querySelector("#card");
		template.innerHTML = template.innerHTML
			.replace(/{{family_name}}/g, card.family)
			.replace(/{{img_src}}/g, card.img_src)
			.replace(/{{name}}/g, card.name)
			.replace(/{{description}}/g, card.description)
			.replace(/{{hp}}/g, card.hp)
			.replace(/{{energy}}/g, card.energy)
			.replace(/{{attack}}/g, card.attack)
			.replace(/{{defense}}/g, card.defense)
			.replace(/{{price}}/g, card.price)
			.replace(/{{id}}/g, card.id)
			.replace(/{{type}}/g, type.toUpperCase());

	});
}

function sell(i) {
	/*	$.ajax({
		type: "POST",
		url: "/cards/"+ i + "/sell"*/
	console.log(i);
	$.ajax({
		type: "POST",
		url: "/cards/" + i + "/sell",
		headers: {
			"Content-Type": "application/json"
		},
		success: function(res) {
			console.log("C\'est vendu !")
			window.location.reload();
		}
	})
}

function buy(i) {
	console.log(i);
	$.ajax({
		type: "POST",
		url: "/cards/" + i + "/buy",
		headers: {
			"Content-Type": "application/json"
		},
		success: function(res) {
			console.log("C\'est acheté !")
			window.location.reload();
		}
	})
}

function logout() {
	$.ajax({
		type: "POST",
		url: "/logout",
		headers: {
			"Content-Type": "application/json"
		},
		success: function() {
			console.log("Déconnecté")
			window.location.href = "/login.html"
		}
	})
}




