package com.asi.controler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asi.dto.CardDTO;
import com.asi.entity.Card;
import com.asi.service.CardService;
import com.asi.service.UserService;

@RestController
public class CardCtrl {
	
	private final CardService cService;
	private final UserService uService;
	
	public CardCtrl(CardService cService, UserService uService) {
		super();
		this.cService = cService;
		this.uService = uService;
	}

	@GetMapping("/cards/buy")
	public List<CardDTO> getCardsToBuy(@CookieValue("sessionCookie") String cookie) throws Exception {
		List<Card> cards = this.cService.showBuy(this.uService.getUserBySurname(cookie));
		return cards.stream().map(CardDTO::new).collect(Collectors.toList());
	}
	
	@GetMapping("/cards/sell")
	public List<CardDTO> getCardsToSell(@CookieValue("sessionCookie") String cookie) {
		List<Card> cards = this.cService.showSell(this.uService.getUserBySurname(cookie));		
		return cards.stream().map(CardDTO::new).collect(Collectors.toList());
	}
	
	@PostMapping("/cards/{id}/sell")
	public void sellCard(@PathVariable int id) {
		this.cService.sell(id);
	}
	
	@PostMapping("/cards/{id}/buy")
	public void buyCard(@PathVariable int id, @CookieValue("sessionCookie") String cookie) {
		this.cService.buy(id, this.uService.getUserBySurname(cookie));
	}

	public CardService getcService() {
		return cService;
	}
	
	
}
