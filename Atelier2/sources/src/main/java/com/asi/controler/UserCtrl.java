package com.asi.controler;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.asi.entity.User;
import com.asi.service.UserService;

@RestController
public class UserCtrl {

	private final UserService uService;
	
	public UserCtrl(UserService uService) {
		this.uService = uService;
	}
	
	@PostMapping("/user")
	public void addUser(@RequestBody User user) {
		this.uService.addUser(user);
	}
	
	
	@GetMapping("/whoAmI")
	public User whoAmI(@CookieValue("sessionCookie") String cookie) {
		return this.uService.getUserBySurname(cookie);
	}
}
