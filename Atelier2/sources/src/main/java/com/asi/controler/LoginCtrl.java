package com.asi.controler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.asi.dto.LoginDTO;
import com.asi.service.UserService;

@RestController
public class LoginCtrl {
	
	private final UserService uService;

	public LoginCtrl(UserService uService) {
		super();
		this.uService = uService;
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginDTO log, HttpServletResponse response) {
		System.out.println(log.getSurname());
		System.out.println(log.getPassword());		
		try {
			String surname = this.uService.login(log.getSurname(), log.getPassword());
			Cookie cookie = new Cookie("sessionCookie", surname);
			cookie.setMaxAge(7*24*360);
			cookie.setSecure(true);
			cookie.setHttpOnly(true);
			cookie.setPath("/");
			response.addCookie(cookie);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/logout")
	public void logout(HttpServletResponse response){
		Cookie cookie = new Cookie("sessionCookie", null);
		cookie.setMaxAge(0);
		cookie.setSecure(true);
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		response.addCookie(cookie);
	}
	
}
