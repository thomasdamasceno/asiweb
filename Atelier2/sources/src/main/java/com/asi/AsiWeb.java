package com.asi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AsiWeb {

	public static void main(String[] args) {
		SpringApplication.run(AsiWeb.class,args);
	}
}
