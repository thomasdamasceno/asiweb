package com.asi.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.asi.entity.User;
import com.asi.repository.UserRepository;

@Service
public class UserService {

	private final UserRepository uRepository;
	private final AuthService aService;
	private final CardService cService;

	public UserService(UserRepository uRepository, AuthService aService, CardService cService) {
		super();
		this.uRepository = uRepository;
		this.aService = aService;
		this.cService = cService;
	}

	public void addUser(User user) {
		user.setPassword(this.aService.hashing(user.getPassword()));
		user.setBalance(5000);
		user = this.uRepository.save(user);
		//Add 5 cards for this user
		for(int i=0; i<5; i++) {
			this.cService.CreateCard(user);
		}
	}

	public void operationBalance(User user, int money) {
		user.setBalance(user.getBalance() + money);
		this.uRepository.save(user);
	}

	public String login(String surname, String password) throws Error {
		password = this.aService.hashing(password);
		List<User> users = this.uRepository.findUserAuth(surname, password);
		if (users.size() > 1) {
			String msg = "Error: Multiple user with the same credential (surname/password)";
			System.out.println(msg);
			throw new Error(msg);
		} else if (users.size() == 0) {
			String msg = "Error: Wrong password or Surname";
			System.out.println(msg);
			throw new Error(msg);
		} else {
			String msg = "Logged in with surname: " + surname + " !";
			System.out.println(msg);
		}
	
		return users.get(0).getSurname();
	}

	public User getUserById(int id) {
		return this.uRepository.findById(id)
				.orElseThrow(() -> new Error("La carte voulue n'a pas été trouvée"));
	}
	
	public User getUserBySurname(String surname) {
		return this.uRepository.findBySurname(surname);
	}
}
