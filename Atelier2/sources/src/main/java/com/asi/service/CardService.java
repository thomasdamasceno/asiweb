package com.asi.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.asi.entity.Card;
import com.asi.entity.Type;
import com.asi.entity.User;
import com.asi.repository.CardRepository;
import com.asi.repository.TypeRepository;

@Service
public class CardService {
	
	private final CardRepository cRepository;
	private final TypeRepository tRepository;
	private final UserService uService;
	

	public CardService(CardRepository cRepository, TypeRepository tRepository, @Lazy UserService uService) {
		this.cRepository = cRepository;	
		this.tRepository = tRepository;
		this.uService = uService;
	}
	
	public void CreateCard(User user) {
		//TODO search for a random type
		Iterable<Type> typesIterable = this.tRepository.findAll();
		List<Type> types = new ArrayList<Type>();
		typesIterable.forEach(types::add);
		Collections.shuffle(types);
		
		this.cRepository.save(new Card(types.get(0), user));
	}

	public void buy(int id_card, User user) {
		Card card = this.cRepository.findById(id_card)
				.orElseThrow(() -> new Error("La carte voulue n'a pas été trouvée"));
		
		this.uService.operationBalance(user, -card.getType().getPrice());
		card.setUser(user);
		this.cRepository.save(card);
	}
	
	public void sell(int id_card) {
		Card card = this.cRepository.findById(id_card)
				.orElseThrow(() -> new Error("La carte voulue n'a pas été trouvée"));
		
		User user = card.getUser();
		
		this.uService.operationBalance(user, card.getType().getPrice());
		card.setUser(null);
		this.cRepository.save(card);
	}
	
	public List<Card> showBuy(User user) {
		List<Card> card = this.cRepository.findMarket(user.getId());		
		return card;
	}
	
	public List<Card> showSell(User user) {
		List<Card> card = this.cRepository.findByUser(user);		
		return card;
	}

}
