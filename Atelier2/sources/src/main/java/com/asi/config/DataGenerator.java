package com.asi.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.asi.entity.Card;
import com.asi.entity.Type;
import com.asi.entity.User;
import com.asi.repository.CardRepository;
import com.asi.repository.TypeRepository;
import com.asi.service.UserService;

@Component
public class DataGenerator implements ApplicationRunner {

	private final TypeRepository tRepository;
	private final CardRepository cRepository;
	private final UserService uService;

	public DataGenerator(TypeRepository tRepository, CardRepository cRepository, UserService uService) {
		this.tRepository = tRepository;
		this.cRepository = cRepository;
		this.uService = uService;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// Create Types
		for (int i = 0; i < 4; i++) {
			Type type = new Type("name" + i, "description" + i, "family" + i, "affinity" + i, i, i, i * 100,
					"https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg");
			this.tRepository.save(type);
		}
		Iterable<Type> typesIterable = this.tRepository.findAll();
		List<Type> types = new ArrayList<Type>();
		typesIterable.forEach(types::add);

		// Create Cards
		for (int i = 0; i < 20; i++) {
			Card card = new Card(types.get(i % 4), null);
			this.cRepository.save(card);
		}

		// Create Users
		for (int i = 0; i < 3; i++) {
			User user = new User("name" + i, "surname" + i, "password" + i);
			this.uService.addUser(user);
		}
	}
}
