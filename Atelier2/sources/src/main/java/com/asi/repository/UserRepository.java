package com.asi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.asi.entity.User;
	
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	@Query("select u from User u where u.surname=?1 and u.password=?2")
	public List<User> findUserAuth(String surname,String password);
	
	public User findBySurname(String surname);
}
