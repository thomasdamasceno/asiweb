package com.asi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.asi.entity.Type;

@Repository
public interface TypeRepository  extends CrudRepository<Type, Integer> {

}
