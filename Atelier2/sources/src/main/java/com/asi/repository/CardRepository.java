package com.asi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.asi.entity.Card;
import com.asi.entity.User;

@Repository
public interface CardRepository  extends CrudRepository<Card, Integer>{
	
	@Query("select c from Card c where c.user.id is null")
	public List<Card> findMarket(int id_user);
	
	public List<Card> findByUser(User user);
	
}