package com.asi.dto;

import com.asi.entity.User;

public class UserDTO {

	private String surname;
	private String name;
	private int balance;

	public UserDTO(User user) {
		super();
		this.surname = user.getSurname();
		this.name = user.getName();
		this.balance = user.getBalance();
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
