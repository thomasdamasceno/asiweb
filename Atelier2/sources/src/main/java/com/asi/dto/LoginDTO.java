package com.asi.dto;

public class LoginDTO {

	private String surname;
	private String password;
	
	public LoginDTO(String surname, String password) {
		super();
		this.surname = surname;
		this.password = password;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
