package com.asi.dto;

import com.asi.entity.Card;

public class CardDTO {

	private int id;
	private String name;
	private String description;
	private String family;
	private String affinity;
	private int energy;
	private int hp;
	private int price;
	private String imageUrl;
	
	public CardDTO(Card card) {
		super();
		this.id = card.getId();
		this.name = card.getType().getName();
		this.description = card.getType().getDescription();
		this.family = card.getType().getFamily();
		this.affinity = card.getType().getAffinity();
		this.energy = card.getType().getEnergy();
		this.hp = card.getType().getHp();
		this.price = card.getType().getPrice();
		this.imageUrl = card.getType().getImageUrl();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getAffinity() {
		return affinity;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}