package com.asi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "UserEntity")
@Entity
public class User {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	@Column(unique = true)
	private String surname;
	private String password;
	private int balance;

	public User() {
	}

	public User(String name, String surname, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.password = password;
		this.balance = 5000;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
